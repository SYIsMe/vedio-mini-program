// app.js
App({
  /**如果电脑和手机不是同一个网段则需要内网穿透 */
  serverUrl: "http://localhost:9005",
  userInfo: null,

  setGlobalUserInfo: function(user) {
    wx.setStorageSync("userInfo", user);
  },

  getGlobalUserInfo: function () {
    return wx.getStorageSync("userInfo");
  },
  
  reportReasonArray: [
    "神勇就是我长得太丑",
    "色情低俗",
    "政治敏感",
    "涉嫌诈骗",
    "辱骂谩骂",
    "广告垃圾",
    "诱导分享",
    "引人不适",
    "过于暴力",
    "违法违纪",
    "其它原因"
  ]

})
