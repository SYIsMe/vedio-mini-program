const app = getApp()

Page({
  data: { 
    // 用于分页的属性
    totalPage: 1,
    page:1,
    videoList:[],

    screenWidth: 350,
    serverUrl: "",

    searchContent: ""
  },

  /**
   * 一到这个页面就会自动搜索查询
   * @param {}} params 
   */
  onLoad: function (params) {
    // 获取系统信息同步接口
    var screenWidth = wx.getSystemInfoSync().screenWidth;
    this.setData({
      screenWidth: screenWidth,
    });

    // 获取搜索后传来的搜索内容参数
    // url: '../index/index?isSaveRecord=1&search=' + value
    var searchContent = params.search;
    var isSaveRecord = params.isSaveRecord;
    if (isSaveRecord == null || isSaveRecord == '' || isSaveRecord == undefined) {
      isSaveRecord = 0;
    }

    this.setData({
      searchContent: searchContent
    });

     // 获取当前的分页数
    var page = this.data.page;
    this.getAllVideoList(page, isSaveRecord);
  },

  /**
   * 向后端发送请求获取所有的视频列表
   */
  getAllVideoList: function(page, isSaveRecord) {
      var me = this;
      var serverUrl = app.serverUrl;

    wx.showLoading({
      title: '请等待，加载中...',
    });

    var searchContent = me.data.searchContent;

    wx.request({
      url: serverUrl + '/video/showAll?page=' + page + "&isSaveRecord=" + isSaveRecord,
      method: "POST",
      data: {
        videoDesc: searchContent
      },
      success:function(res) {
        wx.hideLoading();
        wx.hideNavigationBarLoading();
        wx.stopPullDownRefresh();
          
        console.log(res.data);

        // 判断当前页page是否是第一页，如果是第一页，那么设置videoList为空（为了实现上下拉刷新）
        if (page === 1) {
          me.setData({
            videoList: []
          });
        }

        // 在前端展示后端传来的视频数据
        var videoList = res.data.data.rows;  // 后端的视频列表
        var newVideoList = me.data.videoList;  // 现有的视频列表

        me.setData({
          videoList: newVideoList.concat(videoList),  // 新的拼接上旧的（上拉时要接下去显示加载）
          page: page,
          totalPage: res.data.data.total,
          serverUrl: serverUrl
        });
      }
    })
  },

  /**
   * 官方提供下拉时触发的函数
   */
  onReachBottom: function() {
    var currentPage = this.data.page;
    var totalPage = this.data.totalPage

    // 当前页为最大页时就返回函数不在刷新
    if (currentPage == totalPage) {
      wx.showToast({
        title: '到底了，别翻了~~',
        icon: "none"
      })
      return;
    }

    var page = currentPage + 1;
    
    this.getAllVideoList(page, 0);
  },

  /**
   * 上拉刷新回到第一页
   */
  onPullDownRefresh: function() {
    wx.showNavigationBarLoading();
    this.getAllVideoList(1, 0);
  },

  /**
   * 点击视频后显示视频详情页并播放
   * @param {bindtap='showVideoInfo' data-arrindex='{{index}}} e 
   */
  showVideoInfo: function(e) {
    var me = this;
    var videoList = me.data.videoList;  // 获取当前页面的视频列表
    var arrindex = e.target.dataset.arrindex;
    // 获取对应下标的视频对象，转换为json格式数据后传参给视频详情页
    var videoInfo = JSON.stringify(videoList[arrindex]);

    wx.redirectTo({
      url: '../videoinfo/videoinfo?videoInfo=' + videoInfo
    })
  }
})
