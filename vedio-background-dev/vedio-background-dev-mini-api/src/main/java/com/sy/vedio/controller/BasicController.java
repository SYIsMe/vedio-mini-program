package com.sy.vedio.controller;

import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.sy.utils.RedisOperator;

@RestController
public class BasicController {
	
	@Autowired
	public RedisOperator redis;

	/**
	 * 用户session
	 */
	public static final String USER_REDIS_SESSION = "user-redis-session";

	/**
	 * 文件的命名空间
	 */
	public static final String FILE_SPACE = "D:/小项目/vedio-mini-program/vedio-background-dev/vedio-background-dev-mini-api/video_dev";

	/**
	 *	ffmpeg工具所在目录
	 */
	public static final String FFMPEG_EXE = "C:\\Users\\神勇就是我\\Desktop\\ffmpeg\\bin\\ffmpeg.exe";

	/**
	 * 每页显示的页数
	 */
	public static final Integer PAGE_SIZE = 3;
}
