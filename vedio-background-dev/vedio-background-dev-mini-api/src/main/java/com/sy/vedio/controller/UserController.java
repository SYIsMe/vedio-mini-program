package com.sy.vedio.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import com.sy.utils.Result;
import com.sy.vedio.pojo.UsersReport;
import com.sy.vedio.pojo.vo.PublisherVideo;
import com.sy.vedio.pojo.vo.UsersVO;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.sy.vedio.pojo.Users;
import com.sy.vedio.service.UserService;
import io.swagger.annotations.Api;


@RestController
@RequestMapping("/user")
public class UserController extends BasicController {
	
	@Autowired
	private UserService userService;

	/**
	 * 用户头像图片上传接口
	 * @param userId
	 * @param files
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/uploadFace")
	public Result uploadFace(String userId,
							 @RequestParam("file") MultipartFile[] files) throws Exception {

		// 非空判断
		if (StringUtils.isBlank(userId)) {
			return Result.errorMsg("用户id不能为空...");
		}
		
		// 文件保存的命名空间
		String fileSpace = "D:/小项目/vedio-mini-program/vedio-background-dev/vedio-background-dev-mini-api/video_dev";
		// 保存到数据库中的相对路径
		String uploadPathDB = "/" + userId + "/face";
		
		FileOutputStream fileOutputStream = null;
		InputStream inputStream = null;
		try {
			if (files != null && files.length > 0) {
				
				String fileName = files[0].getOriginalFilename();
				if (StringUtils.isNotBlank(fileName)) {
					// 文件上传的最终保存路径
					String finalFacePath = fileSpace + uploadPathDB + "/" + fileName;
					// 设置数据库保存的路径
					uploadPathDB += ("/" + fileName);
					
					File outFile = new File(finalFacePath);
					if (outFile.getParentFile() != null || !outFile.getParentFile().isDirectory()) {
						// 创建父文件夹
						outFile.getParentFile().mkdirs();
					}
					
					fileOutputStream = new FileOutputStream(outFile);
					inputStream = files[0].getInputStream();
					IOUtils.copy(inputStream, fileOutputStream);
				}
				
			} else {
				return Result.errorMsg("上传出错...");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return Result.errorMsg("上传出错...");
		} finally {
			if (fileOutputStream != null) {
				fileOutputStream.flush();
				fileOutputStream.close();
			}
		}

		Users user = new Users();
		user.setId(userId);
		user.setFaceImage(uploadPathDB);
		userService.updateUserInfo(user);
		
		return Result.ok(uploadPathDB);
	}

	/**
	 * 查询用户信息
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/query")
	public Result query(String userId, String fanId) throws Exception {

		if (StringUtils.isBlank(userId)) {
			return Result.errorMsg("用户id不能为空...");
		}

		Users userInfo = userService.queryUserInfo(userId);
		UsersVO userVO = new UsersVO();
		BeanUtils.copyProperties(userInfo, userVO);

		userVO.setFollow(userService.queryIfFollow(userId, fanId));

		return Result.ok(userVO);
	}

	/**
	 * 查询视频发布者的信息
	 * @param loginUserId
	 * @param videoId
	 * @param publishUserId
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/queryPublisher")
	public Result queryPublisher(String loginUserId,
								 String videoId,
								 String publishUserId) throws Exception {

		if (StringUtils.isBlank(publishUserId)) {
			return Result.errorMsg("");
		}

		// 1. 查询视频发布者的信息
		Users userInfo = userService.queryUserInfo(publishUserId);
		UsersVO publisher = new UsersVO();
		BeanUtils.copyProperties(userInfo, publisher);

		// 2. 查询当前登录者和视频的点赞关系（是否显示红星）
		boolean userLikeVideo = userService.isUserLikeVideo(loginUserId, videoId);

		PublisherVideo bean = new PublisherVideo();
		bean.setPublisher(publisher);
		bean.setUserLikeVideo(userLikeVideo);

		return Result.ok(bean);
	}


	/**
	 * 关注
	 * @param userId
	 * @param fanId
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/beyourfans")
	public Result beyourfans(String userId, String fanId) throws Exception {

		if (StringUtils.isBlank(userId) || StringUtils.isBlank(fanId)) {
			return Result.errorMsg("");
		}

		userService.saveUserFanRelation(userId, fanId);

		return Result.ok("关注成功...");
	}


	/**
	 * 取消关注
	 * @param userId
	 * @param fanId
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/dontbeyourfans")
	public Result dontbeyourfans(String userId, String fanId) throws Exception {

		if (StringUtils.isBlank(userId) || StringUtils.isBlank(fanId)) {
			return Result.errorMsg("");
		}

		userService.deleteUserFanRelation(userId, fanId);

		return Result.ok("取消关注成功...");
	}


	@PostMapping("/reportUser")
	public Result reportUser(@RequestBody UsersReport usersReport) throws Exception {

		// 保存举报信息
		userService.reportUser(usersReport);

		return Result.errorMsg("举报成功，奥利给~~~");
	}
}
