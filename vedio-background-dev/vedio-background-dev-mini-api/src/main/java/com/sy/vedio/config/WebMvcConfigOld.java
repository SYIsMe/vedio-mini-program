//package com.sy.vedio.config;
//
//import com.sy.vedio.interceptor.MiniInterceptor;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
//
//
//@Configuration
//public class WebMvcConfigOld extends WebMvcConfigurerAdapter {
//
//	/**
//	 * 配置SpringBoot静态资源映射
//	 * 注意：后面要加 / 一定要加
//	 * @param registry
//	 */
//	@Override
//	public void addResourceHandlers(ResourceHandlerRegistry registry) {
//		registry.addResourceHandler("/**")
//		.addResourceLocations("classpath:/META-INF/resources/")
//				.addResourceLocations("file:D:/小项目/vedio-mini-program/vedio-background-dev/vedio-background-dev-mini-api/video_dev/");
//	}
//
//
////	/**
////	 * 初始化zookeeper客户端容器
////	 * 	<bean id="ZKCurator" class="com.sy.admin.web.util.ZKCurator" init-method="init">
////	 * 		<constructor-arg index="0" ref="client"></constructor-arg>
////	 * 	</bean>
////	 * @return
////	 */
////	@Bean(initMethod = "init")
////	public ZKCuratorClient zKCuratorClient() {
////		return new ZKCuratorClient();
////	}
//
//	/**
//	 * 放入自定义的连接器到容器中
//	 * @return
//	 */
//	@Bean
//	public MiniInterceptor miniInterceptor() {
//		return new MiniInterceptor();
//	}
//
//
//	/**
//	 * 注册拦截器
//	 * @param registry
//	 */
////	@Override
////	public void addInterceptors(InterceptorRegistry registry) {
////		registry.addInterceptor(miniInterceptor()).addPathPatterns("/user/**")
////												  .addPathPatterns("/video/upload", "/video/uploadCover",
////														           "/video/userLike", "/video/userUnLike",
////														           "/video/saveComment")
////												  .addPathPatterns("/bgm/**")
////												  .excludePathPatterns("/user/queryPublisher");
////
////		super.addInterceptors(registry);
////	}
//}
