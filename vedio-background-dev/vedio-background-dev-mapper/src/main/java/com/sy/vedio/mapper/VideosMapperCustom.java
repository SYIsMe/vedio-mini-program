package com.sy.vedio.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sy.vedio.pojo.Videos;
import com.sy.vedio.pojo.vo.VideosVO;
import com.sy.utils.MyMapper;

public interface VideosMapperCustom extends MyMapper<Videos> {
	
	/**
	 * @Description: 条件查询所有视频列表，因为要取出nickname和imageFace，所以要
	 * 将video表和user表关联查询，自定义sql语句
	 */
	List<VideosVO> queryAllVideos(@Param("videoDesc") String videoDesc, @Param("userId") String userId);

	/**
	 * @Description: 对视频喜欢的数量进行累加
	 */
	void addVideoLikeCount(String videoId);

	/**
	 * @Description: 对视频喜欢的数量进行累减
	 */
	void reduceVideoLikeCount(String videoId);

	/**
	 * @Description: 查询关注的视频
	 */
	List<VideosVO> queryMyFollowVideos(String userId);

	/**
	 * @Description: 查询点赞视频
	 */
	List<VideosVO> queryMyLikeVideos(@Param("userId") String userId);

}