package com.sy.vedio.mapper;

import com.sy.utils.MyMapper;
import com.sy.vedio.pojo.Comments;
import com.sy.vedio.pojo.vo.CommentsVO;

import java.util.List;


public interface CommentsMapperCustom extends MyMapper<Comments> {
	
	public List<CommentsVO> queryComments(String videoId);
}