package com.sy.vedio.mapper;

import com.sy.vedio.pojo.Users;
import com.sy.utils.MyMapper;

public interface UsersMapper extends MyMapper<Users> {

    /**
     * 用户受喜欢数累加
     */
    void addReceiveLikeCount(String userId);

    /**
     * 用户受喜欢数累减
     */
    void reduceReceiveLikeCount(String userId);

    /**
     * 增加粉丝数
     */
    void addFansCount(String userId);

    /**
     * 增加关注数
     */
    void addFollersCount(String userId);

    /**
     * 减少粉丝数
     */
    void reduceFansCount(String userId);

    /**
     * 减少关注数
     */
    void reduceFollersCount(String userId);
}