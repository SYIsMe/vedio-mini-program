package com.sy.vedio.mapper;

import com.sy.vedio.pojo.UsersReport;
import com.sy.utils.MyMapper;

public interface UsersReportMapper extends MyMapper<UsersReport> {
}