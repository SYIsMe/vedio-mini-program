package com.sy.vedio.mapper;

import com.sy.vedio.pojo.SearchRecords;
import com.sy.utils.MyMapper;

import java.util.List;

public interface SearchRecordsMapper extends MyMapper<SearchRecords> {

    List<String> getHotwords();
}