package com.sy.vedio.mapper;


import com.sy.vedio.pojo.Comments;
import com.sy.utils.MyMapper;

public interface CommentsMapper extends MyMapper<Comments> {
}