package com.sy.vedio.service;

import com.sy.vedio.pojo.Users;
import com.sy.vedio.pojo.UsersReport;


public interface UserService {


    /**
     * 查询注册用户是否已存在
     * @param username
     * @return
     */
    boolean isUserExist(String username);

    /**
     * 保存注册用户信息
     * @param user
     */
    void saveUser(Users user);

    /**
     * 用户登录功能，查询用户信息是否正确
     * @param username
     * @param md5Str
     * @return
     */
    Users queryUserForLogin(String username, String md5Str);

    /**
     * 修改用户信息
     *      上传头像后数据更新到数据库中
     * @param user
     */
    void updateUserInfo(Users user);

    /**
     * 查询用户信息
     * @param userId
     * @return
     */
    Users queryUserInfo(String userId);


    /**
     * 查询用户是否喜欢点赞视频
     * @param userId
     * @param videoId
     * @return
     */
    boolean isUserLikeVideo(String userId, String videoId);


    /**
     * 增加用户和粉丝的关系
     * @param userId
     * @param fanId
     */
    void saveUserFanRelation(String userId, String fanId);


    /**
     * 删除用户和粉丝的关系
     * @param userId
     * @param fanId
     */
    void deleteUserFanRelation(String userId, String fanId);


    /**
     * 查询用户是否被关注
     * @param userId
     * @param fanId
     * @return
     */
    boolean queryIfFollow(String userId, String fanId);


    /**
     *  举报用户
     */
    void reportUser(UsersReport userReport);
}
